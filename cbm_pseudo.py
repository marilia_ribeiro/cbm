#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Link artigo: https://drive.google.com/file/d/0BySqE59aBY86VzVWZzFOZTR2bjQ/view

TID - identificação única da trajetória
P - coordenada
T - trajetória
|Ti| - tamanho da traj
FT - trajetória frequente
k - cluster   
	# identificado por indices i,j --> 1 <= i <= m e 1 <= j <= n (onde m x n representa a área retangular do conjunto de trajetórias)
	# numero de cluster inicial é: k = (m x n)/s
s - tamanho do cluster
D = conjunto de dados de trajeorias
'''


'''
Input: A trajectory coordinates database D, cluster size as s and a minimum support be ksi (letra grega).
Output: Set of coordinates where the FT (Fequente Trajectory) of the moving objects pass.

Step 1: Start;
- Creation of clusters. Each cluster has some properties and their respective initial values set like 
count = 0 , previous and next cluster links, active as false and processed as false.

Step 2 : Declare an array A [ ];
- To store active set of clusters.
- Scan the trajectory dataset D and increment the corresponding cluster.

Step 3 : Read a trajectory dataset D;

Step 4 : Find the minimum and maximum area where the trajectories are spanning across x and y axis ;
- To find number of square clusters, of size k x k in the trajectory span area m x n.

Step 5: k =m xn/s;

Step 6 : Create k uniform square clusters of size s with indices Cij;

Step 7 : Repeat;

Step 8 : Assign or reassign each coordinate of the cluster to which the coordinate is the most similar,
based on the mean value of the coordinates in the cluster;

Step 9: Update the cluster means m,J by calculating the mean value of the objects for each cluster Cij;

Step 10 : Calculate E;

Step 11 : Reduce cluster size k = k - k/4 ;

Step 12: Until square error, E converges;
- Select the active set of clusters.

Step 13: Set cas 0;

Step 14: For (i = m_min; i <= m_max; i + k);

Step 15: For (j = n_min; j.<= n_max; j + k);

Step 16: If Cijcount >= min_sup then;

Step 17 : Set Cij active as true;

Step18:A[c] = Cijcount;

Step19:c++;

Step 20 : End if;

Step 21 : Next j;

Step22: Next i;
- Sort the active clusters in array A.

Step 23: For ( i = 0; i < c- 1; i + + ) ;

Step24: For (j = i+1;j < c;j+ + );

Step25: IfA[i] < A[j], then;

Step 26: A[ i] = A[j];

Step 27 : End if;

Step 28: Nextj;

Step 29: Next i;

Step 30 : Header cluster A [ 0];

Step 31 : Current cluster A [ 0 ];
- Loop to link the active set of unlinked clusters from the sorted array A.

Step 32 : Repeat;

Step 33 : From current cluster find the unprocessed and active clusters surrounding the nearest eight
neighborhood clusters having count;;, current cluster's count;

Step 34 : If found then;

Step 35 : If more than one clusters having same count found then;

Step 36 : Choose any cluster arbitratily adjacent to current cluster;

Step 37: End if;

Step 38: Set current cluster to currently found cluster;

Step 39: Unk the current cluster with previous current cluster;

Step 40 : Set current cluster, processed as true;

Step 41: End if;

Step 42 : Check any unprocessed clusters exists in array A[ ] ;

Step 43 : If found then;

Step 44: Repeat Steps 32-48;

Step 45 : Else;

Step 46 : Go to Step 49 ;

Step 47: End if;

Step 48: Until all unprocessed, active set of clusters are processed;

Step 49 : From each header cluster to current cluster collect all the linked clusters in sequence;

Step 50 : End of algorithm.
'''

def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
