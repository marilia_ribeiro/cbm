#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Link artigo: https://drive.google.com/file/d/0BySqE59aBY86VzVWZzFOZTR2bjQ/view

TID - identificação única da trajetória
P - coordenada
T - trajetória
|Ti| - tamanho da traj
FT - trajetória frequente
k - número de clusters
	# identificado por indices i,j --> 1 <= i <= m e 1 <= j <= n (onde m x n representa a área retangular do conjunto de trajetórias)
	# numero de cluster inicial é: k = (m x n)/s
Cij = cluster
t = número total de coordenadas no cluster
s - tamanho do cluster
D = conjunto de dados de trajeorias
'''


'''
Input: A trajectory coordinates database D, cluster size as s and a minimum support be ksi (letra grega).
Output: Set of coordinates where the FT (Fequente Trajectory) of the moving objects pass.


Step 6 : Create k uniform square clusters of size s with indices Cij;

Step 7 : Repeat;

Step 8 : Assign or reassign each coordinate of the cluster to which the coordinate is the most similar,
based on the mean value of the coordinates in the cluster;

Step 9: Update the cluster means mij by calculating the mean value of the objects for each cluster Cij;


Step 21 : Next j;

Step 22: Next i;
# Sort the active clusters in array A.


Step 30 : Header cluster A [0];

Step 31 : Current cluster A [0];
- Loop to link the active set of unlinked clusters from the sorted array A.

Step 32 : Repeat;

Step 33 : From current cluster find the unprocessed and active clusters surrounding the nearest eight
neighborhood clusters having count;;, current cluster's count;

Step 34 : If found then;

Step 35 : If more than one clusters having same count found then;

Step 36 : Choose any cluster arbitratily adjacent to current cluster;

Step 37: End if;

Step 38: Set current cluster to currently found cluster;

Step 39: Unk the current cluster with previous current cluster;

Step 40 : Set current cluster, processed as true;

Step 41: End if;

Step 42 : Check any unprocessed clusters exists in array A[ ] ;

Step 43 : If found then;

Step 44: Repeat Steps 32-48;

Step 45 : Else;

Step 46 : Go to Step 49 ;

Step 47: End if;

Step 48: Until all unprocessed, active set of clusters are processed;

Step 49 : From each header cluster to current cluster collect all the linked clusters in sequence;

Step 50 : End of algorithm.

'''

def cbm(trajectories, size_cluster, min_sup):
	# valores iniciais dos clusters
	count = 0
	previous_cluster  = None #ponteiro - criar objeto
	next_cluster = None #ponteiro - criar objeto
	active = False
	processed = False
	

	A = [] #clusters ativos
	
	#ler arquivo de trajetórias
	while i < len(trajectories):
		j = 0
		while j < len(trajectories[i]):
		'''encontrar o valor máximo e mínimo de x e y ao ler todas as coordenadas'''
		'''
		- encontrar a área mínimo e máximo, onde as trajetórias se estendem nos exos x e y (mean_center ???)
		- encontrar o número de clusters quadrados, de tamanho k x k na área de extensão da trajetória
		'''
		m_x_n = ??? #área (como calculo essa área?)
		k = m_x_n/size_cluster

	
		#steps 6 (saber se fica dentro ou fora do while) - acho que é dentro, pois o passo 7 manda repetir
		C[i][j] = criaClusters(trajectories, k, size_cluster)
		
	# k-means http://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
	#step 8 - associar cada coordenada ao respectivo cluster, lembrando que deve obedecer o tamanho do cluster (tem que percorrer a base de dados tudo de novo?)
	
	#step 9
	
	E = calcule_E()
	
	k = (k - k)/4
	
	# step 12

	c = 0
	
	for (i = m_min; i <= m_max; i + k):
		for (j = n_min; j.<= n_max; j + k):
			if len(C[i][j]) >= min_sup:
				C[i][j] = True				
				A[c] = len(C[i][j])
				c += 1	


	for (i = 0; i < c - 1; i ++):
		for (j = i + 1; j < c; j++ ):
			if A[i] < A[j]:
				A[i] = A[j]
				

	#step 30 - 32
	
	#step 33 é isso mesmo?
	founds = []	
	for cluster in A:
		if A[i].active == True and A[i].process == False:
			founds[i] = A[i]
	
	if founds:
		if count > 1:
			#step 36		
		#setp 38 - 40
	
	for i in A:
		if A[i].process == False:
			if A[i] in founds:
				#repeat steps 32-48
			else:
				# go to step 49
		#step 48
	
	
	#step 49-50
		
		
	
	#step  - 50

def calcule_E():
	'''
	square-error function
	http://stackoverflow.com/questions/16774849/mean-squared-error-in-numpy
	http://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html
	'''	
	from sklearn.metrics import mean_squared_error
	return mean_squared_error(A, B)

def main(args):
	
	#trajectories = D
	#size_cluster = s
	#min_sup = ksi

	cbm(trajectories, size_cluster, min_sup)
	
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
