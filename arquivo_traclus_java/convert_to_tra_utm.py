#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import utm
import utm

def cria_tra(arquivo, trajetorias):
    arquivo = arquivo.split('.')
    arquivo = ''.join((arquivo[0], '_utm.tra'))
    arquivo = open(arquivo, 'w')

    dimensao = 2
    numero_trajetorias = len(trajetorias)
    
    
    texto = '%s\n%s\n' %(dimensao, numero_trajetorias)
    
    for i, trajetoria in enumerate(trajetorias):
        #numero_pontos = len(trajetoria)/2
        numero_pontos = len(trajetoria)/4
        trajetoria = str(trajetoria).replace('[','')
        trajetoria = str(trajetoria).replace(']','')
        trajetoria = str(trajetoria).replace(', ',' ')
        
        #trajetoria = ' '.join(trajetoria)
        linha = '%s %s %s\n' %(i, numero_pontos, trajetoria)
        print(linha)
        texto = ''.join((texto,linha))

    print(texto)
    arquivo.write(texto)
    #arquivo.close()

    
    
def le_arquivo(arquivo):
    arquivo = open(arquivo).read()    
    
    linhas = arquivo.split('\n')
    linhas = linhas[:-1]
    numero_trajetorias = len(linhas)
    dimensao = 2

    trajetorias = []
    for i, linha in enumerate(linhas):
        linha = linha.split(' = [')[1]
        linha = linha.split(']')[0]
        linha = linha.split(', ')
        trajetorias.append(monta_coordenada(linha))
        #trajetorias.append((i,linha))
        #trajetorias.append(linha)

    #print(trajetorias)
    
    return trajetorias
        
def monta_coordenada(trajetoria):
    trajetoria_utm = []
    
    latitude, longitude = 0.0, 0.0
    coordenada_utm = 0
    for i in range(len(trajetoria)+1):
        if i >= 1:     
            if i-1 % 2 == 0:
                longitude = float(trajetoria[i-1])
                latitude = float(trajetoria[i]) 
            coordenada_utm = utm.from_latlon(latitude, longitude)
            trajetoria_utm.append(coordenada_utm[0])
            trajetoria_utm.append(coordenada_utm[1])
            
    #print(trajetoria_utm)
    return trajetoria_utm
    #return coordenada_utm

    
def main(args):
    arquivo = 'itaum2.txt'
    trajetorias = le_arquivo(arquivo)
    cria_tra(arquivo, trajetorias)
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
