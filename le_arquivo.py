#!/usr/bin/env python
# -*- coding: utf-8 -*-
def cria_json(arquivo, trajetorias):
	arquivo = arquivo.split('.')
	arquivo = ''.join((arquivo[0], '_json.', arquivo[1]))
	arquivo = open(arquivo, 'w')
	
	json = {'trajectories': trajetorias}
	arquivo.write(str(json))
	#arquivo.close()

	
	
def le_arquivo(arquivo):
	arquivo = open(arquivo).read()
	arquivo = arquivo.replace(' ', '')
	#print(arquivo)
	
	linhas = arquivo.split('\n')
	linhas = linhas[:-1]
	
	#print(linhas)
	
		
	trajetorias = []
	for i, linha in enumerate(linhas):
		linha = linha.split('[')[1]
		linha = linha.split(']')[0]		
		trajetoria = linha		
		trajetoria = trajetoria.replace('(', '[')
		trajetoria = trajetoria.replace(')', ']')
		trajetorias.append(trajetoria)
		#trajetoria = trajetoria.split('(')[1:]
		#trajetoria = trajetoria.split('),')
		#print(trajetoria)
		
		
	for trajetoria in trajetorias:
		#print(trajetoria)
		for i, ponto in enumerate(trajetoria):
			print(ponto)
		
		
	
def monta_coordenada(trajetoria):
	coordenadas = {}
	latitude, longitude = 0.0, 0.0
	for i in range(len(trajetoria)):
		if i % 2 == 0:
			coordenadas["x"] = float(trajetoria[i])
		else:
			coordenadas["y"] = float(trajetoria[i])
	return coordenadas

def clean_coords(c):
    i = 0
    lat = 0
    resultado = []
    while i < len(c):
        if i % 2 == 0:
            long = c[i]
        else:
            lat = c[i]
            resultado.append((lat, long))
        i += 1

	
    return resultado
    
def main(args):
	arquivo = 'trajetorias_cbm.txt'
	le_arquivo(arquivo)
	#trajetorias = le_arquivo(arquivo)
	#cria_json(arquivo, trajetorias)
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
