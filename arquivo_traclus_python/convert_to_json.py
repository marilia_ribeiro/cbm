#!/usr/bin/env python
# -*- coding: utf-8 -*-
def cria_json(arquivo, trajetorias):
    arquivo = arquivo.split('.')
    arquivo = ''.join((arquivo[0], '_tra.', arquivo[1]))
    arquivo = open(arquivo, 'w')

    # tratando o texto - tirando as aspas símples do vetor
    trajetorias = str(trajetorias).replace("'","")
    #print(trajetorias)
    
    json = {"trajectories": trajetorias}
    arquivo.write(str(json))
    #arquivo.close()    
    
def le_arquivo(arquivo):
    arquivo = open(arquivo).read()
    
    linhas = arquivo.split('\n')
    linhas = linhas[:-1]
        
    trajetorias = []
    for i, linha in enumerate(linhas):
        linha = linha.split(' = [')[1]
        linha = linha.split(']')[0]
        trajetoria = linha
        trajetoria = trajetoria.split(', ')
        coordenadas=(monta_coordenada(trajetoria))
        trajetorias.append(coordenadas)

    
    #print(trajetorias)
    return trajetorias      
        
def monta_coordenada(trajetoria):
    coordenadas = []
    latitude, longitude = 0.0, 0.0
    
    for i in range(len(trajetoria)+1):
        if i >= 1:
            if ((i-1) % 2) == 0:
                longitude = (trajetoria[i-1])
                latitude = (trajetoria[i])
                coordenadas.append('{"x": %s, "y": %s}' %(latitude, longitude))
                #print('(%s, {"x": %s, "y": %s})' %(i, latitude, longitude))
    #print(coordenadas)
    return coordenadas

def main(args):
    arquivo = 'itaum.txt'
    trajetorias = le_arquivo(arquivo)
    cria_json(arquivo, trajetorias)
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
